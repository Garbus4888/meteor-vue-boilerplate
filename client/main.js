import { Meteor } from 'meteor/meteor';
import { Router } from 'meteor/akryum:vue-router';

import AppLayout from '/imports/ui/AppLayout.vue';

const router = new Router({
  history: true,
  saveScrollPosition: true
});

Meteor.startup(() => {
  router.start(AppLayout, 'app');
});
